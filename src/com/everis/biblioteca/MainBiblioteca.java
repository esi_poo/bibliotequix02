package com.everis.biblioteca;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 * Software de gesti�n de bibliotecas
 * 
 * @author everis
 */
public class MainBiblioteca {


	public static void main(String[] args) {
		
		Usuario usuario1 = new Usuario("12345678s", "Javier", "Martinez", "Mellado", "Mi casa", "626707017", "hola", 1);
		Usuario usuario2 = new Usuario("12345678s", "Vanya Mitkova", "Ivanova", "", "Su casa", "null", "hola", 1);
		Usuario usuario3 = new Usuario("12345678s", "Julian", "Hernandez", "Carrion", "Su casa", "null", "hola", 1);
		Usuario usuario4 = new Usuario("12345678s", "Samuel", "Hernandez", "Carrion", "Su casa", "null", "hola", 1);

		
		int []telefono = {0,1,2,3,4,5,6,7,8};

        
        Editorial edit1 = new Editorial(22, "Manolita", "Su calle piso bajo", telefono, "Jos�");

        Biblioteca bib = new Biblioteca(1, "Biblioteca everis Murcia",
                1/* "Murcia" */, 1234567890, "HPC Murcia", 1);
        Editorial edit = new Editorial(99, "Editorial Everiensis", "Lorca", telefono, "Salva");
        bib.AnadirAuthor("123456", "Nombre", "apellido1", "apellido2", new Date(), "cuidad",
                " descripcionAuthor");
        bib.AnadirAuthor("123456", "Nombre", "apellido1", "apellido2", new Date(), "cuidad",
                " descripcionAuthor");
        
        // Fechas:
        Date f1 = new Date();
        Date f2 = new Date();
        try {
            SimpleDateFormat dateformat = new SimpleDateFormat("dd/M/yyyy");
            f1 = dateformat.parse("01/01/2015");
            f2 = dateformat.parse("01/12/2015");
        } catch (ParseException e) {
            System.out.println("Problema con la conversi�n de fechas: " + e.getMessage());
            System.exit(-1);
        }

        bib.AnadirLibro("1", "Aprender Git", "Hanae", f1, 1, 2, 2, 1001001001, 20, true, f2);

        Scanner myScanner = new Scanner(System.in);

        int opcion = 0;

        do {
            // MEN�:
            System.out.println(" # ############################################ #");
            System.out.println(" #                  BIBLIOTEQUIX:               #");
            System.out.println(" # ############################################ #");
            System.out.println(" #  Seleccione una opci�n:                      #");
            System.out.println(" #  1. Listar libros en estado disponible       #");
            System.out.println(" #  2. Editorial                                #");
            System.out.println(" #  3. Listar humanos						    #");
            System.out.println(" #  3. Listar autores						    #");
            System.out.println(" #  3. Listar editoriales					    #");
            System.out.println(" #  9. Salir                                    #");
            System.out.println(" # ############################################ #");

            opcion = myScanner.nextInt();

            switch (opcion) {
            case 1: // Listar libros disponibles:
                bib.ListarLibro3(2);
                System.out.println("");
                break;
            case 2: // Listar Editorial:
                System.out.println(edit.getNombreEditorial());
                System.out.println("");
                break;
            case 3: // Listar Usuarios
                System.out.println(usuario1.getNombre());
                System.out.println(usuario2.getNombre());
                System.out.println(usuario3.getNombre());
                break;
            case 4: // Listar Autores
                bib.ListarAuthors();

                break;
            case 5: // Listar editoriales
                System.out.println(edit1);

                break;
            default:
                System.out.println(" #  --> Opci�n incorrecta, seleccione otra opci�n:");
                System.out.println("");
            }
        } while (opcion != 9);

        // Mensaje de FIN:
        System.out.println(" # ############################################ #");
        System.out.println(" #              FIN    BIBLIOTEQUIX:            #");
        System.out.println(" # ############################################ #");

        // Cerrar elementos abiertos
        try {
            myScanner.close();
        } catch (Exception ex) {
            System.out.println(" # Error al cerrar 'myScanner': " + ex.getMessage());
            System.exit(-2);
        }
    }

}
